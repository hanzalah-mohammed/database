package jdbc.dao;

import jdbc.pojo.User;

public interface UserDaoInterface {

    // Admin authentication
    boolean authenticateAdmin(String adminID, String adminPassword);

    // DB methods
    void createUser(User refUser);
    void getAllUsers();
    void updateUser(String userID, String newPassword);
    void deleteUser(String userID);

    // DB close connection
    void closeConnection();
}
