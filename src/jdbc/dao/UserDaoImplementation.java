package jdbc.dao;

import jdbc.controller.UserController;
import jdbc.pojo.User;
import utility.DBUtility;

import java.sql.*;

public class UserDaoImplementation implements UserDaoInterface{

    Connection refConnection = DBUtility.getConnection();
    PreparedStatement ps = null;

    @Override
    public boolean authenticateAdmin(String adminID, String adminPassword) {
        try {
            String sqlQuery = "SELECT * FROM admin WHERE adminID=?";
            ps = refConnection.prepareStatement(sqlQuery); // Take the SQL Query
            ps.setString(1, adminID); // Take parameters needed for the query -> (?)
            ResultSet refResult = ps.executeQuery();
            if (refResult.next()) {
                String resultID = refResult.getString(1);
                String resultPassword = refResult.getString(2);
                if (adminID.equals(resultID) && adminPassword.equals(resultPassword)) {
                    System.out.println("Login Successful");
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    } // end of authenticateAdmin()

    @Override
    public void createUser(User refUser) {

        try {
            String sqlQuery = "insert into user(userID,userPassword) values(?,?)";
            ps = refConnection.prepareStatement(sqlQuery);
            ps.setString(1, refUser.getUserID());
            ps.setString(2, refUser.getUserPassword());
            ps.execute();
            System.out.println("new record has been successfully inserted");
        } catch (SQLException e) {
            System.err.println("Error!!! Record cannot be inserted.");
            e.printStackTrace();
        } // end of catch
    } // end of createUser() method

    @Override
    public void getAllUsers() {

        int total = 0; // to store the total number of records

        try {
            String sqlQuery = "SELECT * FROM user";
            ps = refConnection.prepareStatement(sqlQuery);
            ResultSet refResult = ps.executeQuery();
            System.out.printf("%-10s %2s \n", "User ID", "Password");
            System.out.println("====================");
            while (refResult.next()) { // use while loop if the records are more than 1.
                System.out.printf("%-10s %2s \n", refResult.getString("userID"),
                        refResult.getString("userPassword"));
                System.out.println("--------------------");
                total++;
            }
            String record = total == 1 ? "record" : "records";
            System.out.println(total + " " + record + " have been successfully retrieved.");

        } catch (SQLException e) {
            System.err.println("Error!!! Your operation is unsuccessful.");
        }
    } // end of getAllUsers

    @Override
    public void updateUser(String userID, String newPassword) {

        try {
            String sqlQuery = "UPDATE user SET userPassword=? WHERE userID=?";
            ps = refConnection.prepareStatement(sqlQuery);
            ps.setString(1, newPassword);
            ps.setString(2, userID);
            if (ps.executeUpdate() > 0) { // executeUpdate will return int for rows affected
                System.out.println(userID + " record has been successfully updated.");
            } else {
                throw new Exception("User " + userID + " not found!! Operation is unsuccessful.");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    } // end of updateUser()

    @Override
    public void deleteUser(String userID) {
        try {
            String sqlQuery = "DELETE FROM user where userID = ?";
            ps = refConnection.prepareStatement(sqlQuery);
            ps.setString(1, userID);
            if (ps.executeUpdate() > 0) { // use the if loop if the record is only 1
                System.out.println(userID + " record has been successfully deleted");
            } else { // if the record is empty or invalid input
                throw new Exception("User " + userID + " not found!! Operation is unsuccessful.");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } // end of catch
    } // end of deleteUser()

    @Override
    public void closeConnection(){ // close connection to DB
        try {
            refConnection.close();
            System.out.println("Connection closed.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    } // end of closeConnection()
}
