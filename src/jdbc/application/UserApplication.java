package jdbc.application;

import jdbc.controller.UserController;
import utility.DBUtility;

import java.sql.*;
import java.util.Scanner;

public class UserApplication {
    public static void main(String[] args){
        UserController refController = new UserController();
        refController.adminLogin();
    }
}
