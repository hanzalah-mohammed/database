package jdbc.service;

import jdbc.dao.UserDaoImplementation;
import jdbc.dao.UserDaoInterface;
import jdbc.pojo.User;

import java.util.Scanner;

public class UserServiceImplementation implements UserServiceInterface{

    UserDaoInterface refInterface = new UserDaoImplementation();
    Scanner refScanner = new Scanner(System.in);

    @Override
    public void adminLogin() {

        boolean isAuthenticated = false;

        while (!isAuthenticated) {
            try {
                System.out.println("Enter User ID: ");
                String adminID = refScanner.next();

                System.out.println("Enter new password: ");
                String adminPassword = refScanner.next();

                if (refInterface.authenticateAdmin(adminID, adminPassword)) {
                    isAuthenticated = true;
                    userChoice();
                } else {
                    throw new Exception("Invalid Credentials!! \n");
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void userChoice() { // only once the admin is authenticated

        boolean sessionActive = true; // always be true until the user logout.

        while (sessionActive) { // will always run this loop while sessionActive is true
            try {
                System.out.println("\n User Database: \n 1) Create New User \n " +
                        "2) Get All Users \n " +
                        "3) Update User \n " +
                        "4) Delete User \n" +
                        " 5) Logout \n");
                System.out.println("Enter you choice: ");

                int userChoice = refScanner.nextInt();

                if (userChoice == 1) {
                    createNewUser();
                } else if (userChoice == 2) {
                    getAllUsers();
                } else if (userChoice == 3) {
                    updateUser();
                } else if (userChoice == 4) {
                    deleteUser();
                } else if (userChoice == 5) {
                    sessionActive = false; // to stop from running this while loop
                    refInterface.closeConnection();
                } else { // for inputs other than 1-5
                    throw new Exception("Invalid input!!");
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    } // end of userChoice() method

    public void createNewUser() {

        System.out.println("Enter User ID: ");
        String userLoginID = refScanner.next();

        System.out.println("Enter User Password: ");
        String userPassword = refScanner.next();

        User refUser = new User();
        refUser.setUserID(userLoginID);
        refUser.setUserPassword(userPassword);
        refInterface.createUser(refUser);

    } // endOfUserInsertRecord

    void getAllUsers() {
        refInterface.getAllUsers();
    }

    void updateUser() {
        System.out.println("Enter User ID: ");
        String userID = refScanner.next();

        System.out.println("Enter new password: ");
        String newPassword = refScanner.next();

        refInterface.updateUser(userID, newPassword);
    }

    void deleteUser() {
        System.out.println("Enter User ID to delete: ");
        String input = refScanner.next();

        refInterface.deleteUser(input);
    }
}
